package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Security unit tests")
@Tag("security")
public class appSecuritySpec {

    @Test
    public void BiggerThanIntMaxSizeNeedsApproval() {
        assertThrows(IllegalArgumentException.class, () -> {
            Main app = new Main();
            boolean res = app.approval(Amount.of("2147483648"));
            assertTrue(res,() -> "Bigger than int max size needs approval");
        });
    }

    @Test
    public void LessThanIntMinSizeNeedsApproval() {
        assertThrows(IllegalArgumentException.class, () -> {
            Main app = new Main();
            boolean res = app.approval(Amount.of("-2147483649"));
            assertTrue(res,() -> "Less than int min size needs approval");
        });
    }
}
