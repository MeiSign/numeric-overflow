package app;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AmountTest {

    @Test
    public void shouldThrowIllegalArgumentExceptionForPositiveOverflow() {
        assertThrows(IllegalArgumentException.class, () -> { Amount.of(String.valueOf(Integer.MAX_VALUE + 1L)); });
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionForNegativeOverflow() {
        assertThrows(IllegalArgumentException.class, () -> Amount.of(String.valueOf(Integer.MIN_VALUE - 1L)));
    }
}
