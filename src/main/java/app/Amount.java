package app;

import java.math.BigInteger;

public class Amount {
    private final BigInteger amount;

    private Amount(BigInteger amount) {
        if (amount.compareTo(BigInteger.valueOf(Integer.MAX_VALUE)) > 0 ||
                amount.compareTo(BigInteger.valueOf(Integer.MIN_VALUE)) < 0) {
            throw new IllegalArgumentException("Invalid amount");
        }

        this.amount = amount;
    }

    public boolean isGreaterThan(int x) {
        return amount.compareTo(BigInteger.valueOf(x)) >= 0;
    }

    public static Amount of(String amount) {
        return new Amount(new BigInteger(amount));
    }
    @Override
    public String toString() {
        return "Amount(" + amount + ")";
    }
}
